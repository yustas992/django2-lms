from django.contrib import admin

from teachers.models import Teacher, Course

admin.site.register(Teacher)
admin.site.register(Course)

