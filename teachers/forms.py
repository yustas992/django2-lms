import re

import django_filters
from django.core.exceptions import ValidationError
from django.forms import ModelForm

from teachers.models import Teacher


class TeacherFilter(django_filters.FilterSet):
    class Meta:
        model = Teacher
        fields = {
            'first_name': ['iexact'],
            'last_name': ['iexact'],
            'it_experience': ['exact']
        }


class TeacherBaseForm(ModelForm):
    class Meta:
        model = Teacher
        fields = '__all__'

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']
        if not re.match(r'(\+\d\d?)?\(\d{3}\)(\d-?){7}$', phone_number):
            raise ValidationError('Phone number should be in'
                                  'format +1(121)323-32-23')
        return self.cleaned_data['phone_number']

    def clean_email(self):
        ignore_email = ['yandex.ua', 'vk.com']
        email = self.cleaned_data['email']
        if not re.search(r'@(yandex\.ua|vk\.com)$', email):
            return self.cleaned_data['email']
        else:
            raise ValidationError(f'Email should not be in {ignore_email}')


class TeacherCreateForm(TeacherBaseForm):
    pass


class TeacherUpdateForm(TeacherBaseForm):
    class Meta:
        model = Teacher
        fields = '__all__'
        exclude = ['it_experience']
