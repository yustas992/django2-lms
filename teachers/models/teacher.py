from django.db import models
from faker import Faker
import random
from core_lms.models import Person


class Teacher(Person):
    it_experience = models.IntegerField(null=True)
    birth_date = models.DateTimeField(null=True, blank=True)
    group = models.ForeignKey(
        to='groups.Group',
        null=True,
        on_delete=models.SET_NULL,
        related_name='teachers'
    )
    courses = models.ManyToManyField(
        to='teachers.Course',
        related_name='courses'
    )

    @classmethod
    def get_teachers(cls, count):
        fake = Faker()
        for _ in range(count):
            t = Teacher()
            t.first_name = fake.first_name()
            t.last_name = fake.last_name()
            t.email = fake.email()
            t.phone_number = fake.phone_number()
            t.it_experience = random.randint(1, 10)
            t.birth_date = fake.date_time()

            t.save()

    def __str__(self):
        return f'Teacher({self.id}) {self.first_name} ' \
               f'{self.last_name} (IT experience {self.it_experience} y.)' \
               f'{self.email},{self.phone_number},{self.birth_date}'

    @property
    def name(self):
        return f'{self.first_name} {self.last_name}'
