from django.db import models


class Course(models.Model):
    name = models.CharField(max_length=64, null=False)

    def __str__(self):
        return self.name
